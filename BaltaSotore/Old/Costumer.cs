using System;

namespace BaltaStore.Domain.StoreContext
{


    // interface é somente um contrato que quem for implementar 
    // ela precisa ter
    public interface IPerson
    {
        string Name { get; set; }
        DateTime BirthDate { get; set; }

        void OnRegister();
    }

    public interface IEmployer
    {

        decimal Salary { get; set; }

    }




    // Para proibir herança de classe pode usar Selad apos classs
    // Para impedir a instcia de uma classse usa abstract.
    // para usar ela será necessario fazer herança* Exempl
    // reserva de sala não e algo fisico

    public abstract class Person
    {


        //Propriedades do cliente
        public string Name { get; set; }

        public DateTime BirthDatte { get; set; }
        public decimal Salary { get; set; }
    }

    public class Customer : IPerson, IEmployer
    // herdando os dados de Person

    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public decimal Salary { get; set; }


        //Metodos
        public void OnRegister() { }

// sobrescerver metodo assim ao chamar Customer.ToString()
//  ele retorna o nome
        public override string ToString()
        {
            return Name;
        }

    }




}