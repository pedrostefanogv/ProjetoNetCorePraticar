namespace BaltaStore.Shared.Commands
{

    // permitir criar o handler command somente se for um command
    public interface ICommandResult
    {
        bool Success { get; set; }
        string Message { get; set; }
        object Data { get; set; }


    }


}