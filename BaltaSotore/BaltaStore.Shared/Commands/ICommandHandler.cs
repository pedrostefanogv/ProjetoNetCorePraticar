namespace BaltaStore.Shared.Commands
{

// permitir criar o handler command somente se for um command
    public interface ICommandHandler<T> where T : ICommand
    {

        ICommandResult Handle(T command);
        
        
    }


}