using System;
using System.Collections.Generic;
using BaltaStore.Shared.Commands;
using FluentValidator;
using FluentValidator.Validation;

namespace BaltaStore.Domain.StoreContext.OrderCommands.Inputs
{

    public class PlaceOrderCommand : Notifiable, ICommand
    {



        public PlaceOrderCommand()
        {
            OrderItens = new List<OrderItemCommand>();

        }

        public Guid Customer { get; set; }
        public IList<OrderItemCommand> OrderItens { get; set; }


        public bool Validate()
        {

            AddNotifications(new ValidationContract()
                  .Requires()
                  .HasLen(Customer.ToString(), 36, "Customer", "Identificador do cliente invalido")
                  .IsGreaterThan(OrderItens.Count, 0, "Items", "Nenhum item do pedido foi encontrado")

               );

           return Valid;
        }


        public class OrderItemCommand
        {
            public Guid Product { get; set; }
            public decimal Quantity { get; set; }

        }

    }


}