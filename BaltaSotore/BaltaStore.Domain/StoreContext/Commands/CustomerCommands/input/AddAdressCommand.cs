using System;
using BaltaStore.Domain.StoreContext.Enums;
using BaltaStore.Shared.Commands;
using FluentValidator;
using FluentValidator.Validation;


namespace BaltaStore.Domain.StoreContext.CustomerCommands.Inputs
{

    public class AddAdressCommand : Notifiable, ICommand
    {

        public Guid Id { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public EAddressType Type { get; set; }

        public bool Validate()
        {
            AddNotifications(new ValidationContract()
                .HasMinLen(Street, 3, "Street", "Rua deve conter nominino 3 carateres")
                .HasLen(District, 4, "District", "Cidade deve conter no minimo 4 caracter")
            );
            return Valid;
        }

        bool ICommand.Validate()
        {
            return Valid;
        }



    }


}