using BaltaStore.Shared.Commands;
using FluentValidator;
using FluentValidator.Validation;
using System;

namespace BaltaStore.Domain.StoreContext.CustomerCommands.Inputs
{

    public class CreateCustomerCommand : Notifiable, ICommand
    {
        //FailFastValidation


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Document { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public bool Validate()
        {
            AddNotifications(new ValidationContract()
               .HasMinLen(FirstName, 3, "FistName", "Nome deve conter nominino 3 carateres")
               .HasMinLen(LastName, 3, "LastName", "Sobre nome deve conter nominino 3 carateres")
               .HasMaxLen(FirstName, 30, "LastName", "Nome deve contermaximo 30 caracters")
               .HasMaxLen(LastName, 30, "LastName", "Sobre deve contermaximo 30 caracters")
               .IsEmail(Email, "Email", "O email é invalido")

            );
            return Valid;
        }

        // se usuario exite no banco (Email)
        // se usuario exite no banco (CPF)








    }


}