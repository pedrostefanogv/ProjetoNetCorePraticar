using System;
using FluentValidator;
using BaltaStore.Shared.Commands;
using BaltaStore.Domain.StoreContext.Services;
using BaltaStore.Domain.StoreContext.Entities;
using BaltaStore.Domain.StoreContext.Repositories;
using BaltaStore.Domain.StoreContext.ValueObjects;
using BaltaStore.Domain.StoreContext.CustomerCommands.Inputs;
using BaltaStore.Domain.StoreContext.CustomerCommands.Outputs;

namespace BaltaStore.Domain.StoreContext.Handlers
{

    public class CustomerHandler :
          Notifiable,
          ICommandHandler<CreateCustomerCommand>,
          ICommandHandler<AddAdressCommand>
    {

        private readonly ICustomerRepository _repository;
        private readonly IEmailService _emailservice;




        public CustomerHandler(ICustomerRepository repository, IEmailService emailservice)
        {
            _repository = repository;
            _emailservice = emailservice;



        }



        public ICommandResult Handle(CreateCustomerCommand command)
        {
            // Verifca se CPF já existe
            if (_repository.CheckDocument(command.Document))
                AddNotification("Document", "Este CPF já esta em uso");

            // verifica se email já existe
            if (_repository.CheckEmail(command.Email))
                AddNotification("Email", "Esse email já esta cadastrado");

            //Criar vos
            var name = new Name(command.FirstName, command.LastName);
            var document = new Document(command.Document);
            var email = new Email(command.Email);

            //criar entidades
            var customer = new Customer(name, document, email, command.Phone);

            // Validar entidades
            AddNotifications(Notifications);
            if (Invalid)
                return new CommandResult(false, "Corriga campos abaixo", Notifications);

            // Persistir dados
            _repository.Save(customer);

            // enviar email
            _emailservice.Send(email.Address, "hello@com.br", "Bem Vindo", "Seja bem vindo ");

            //retornar resut para tela

            return new CommandResult(true, "Bem Vindo ao Balta", new
            {
                Id = customer.Id,
                Name = name.ToString(),
                Email = email.Address
            });




        }


        public ICommandResult Handle(AddAdressCommand command)
        {
            throw new System.NotImplementedException();
        }



    }


}