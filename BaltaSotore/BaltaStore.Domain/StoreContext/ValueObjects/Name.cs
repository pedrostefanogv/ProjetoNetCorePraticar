using FluentValidator;
using FluentValidator.Validation;

namespace BaltaStore.Domain.StoreContext.ValueObjects
{
    public class Name : Notifiable
    {

        public Name(string firstName, string lastname)
        {

            FirstName = firstName;
            LastName = lastname;
            new ValidationContract()
            .Requires()
            .HasMinLen(firstName,3,"FistName","Nome deve conter nominino 3 carateres")
            .HasMinLen(lastname,3,"LastName","Sobre nome deve conter nominino 3 carateres")
            .HasMaxLen(firstName,30,"LastName","Nome deve contermaximo 30 caracters")
            .HasMaxLen(lastname,30,"LastName","Sobre deve contermaximo 30 caracters");

        }


        public string FirstName { get; private set; }
        public string LastName { get; private set; }


        public override string ToString()
        {

            return  $"{FirstName} {LastName}";

        }




    }


}