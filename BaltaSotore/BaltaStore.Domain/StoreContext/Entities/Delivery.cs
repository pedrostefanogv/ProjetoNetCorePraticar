using System;
using  BaltaStore.Domain.StoreContext.Enums;
using BaltaStore.Shared.Entities;

namespace BaltaStore.Domain.StoreContext.Enums
{
    public class Delivery : Entity
    {
        public Delivery(DateTime estimatedDeliveryDaate)
        {
            CreateDate = DateTime.Now;
            EstimatedDeliveryDaate = estimatedDeliveryDaate;
            Status = EDeliveryStatus.Waiting;
        }

        public DateTime CreateDate { get; private set; }

        public DateTime EstimatedDeliveryDaate { get; private set; }
        public EDeliveryStatus Status { get; private set; }




        public void Ship()
        {
            // se data estimada de entrega for no passado não entregar
            Status = EDeliveryStatus.Shiped;

        }

        public void Cancel()
        {
            // se status já tiver sido cancelado não pode cancelar
            Status = EDeliveryStatus.Cancelad;

        }




    }
}