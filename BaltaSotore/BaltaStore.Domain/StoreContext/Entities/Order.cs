using System;
using System.Collections.Generic;
using System.Linq;
using BaltaStore.Domain.StoreContext.Enums;
using BaltaStore.Shared.Entities;
using FluentValidator;

namespace BaltaStore.Domain.StoreContext.Entities
{
    public class Order :  Entity

    {
        private readonly IList<OrderItem> _itens;
        private readonly IList<Delivery> _delivery;


        public Order(Customer customer)
        {

            Customer = customer;


            CreateDate = DateTime.Now;
            Status = EOrderStatus.Created;
            _itens = new List<OrderItem>();
            _delivery = new List<Delivery>();

        }


        public Customer Customer { get; private set; }
        public string Number { get; private set; }
        public DateTime CreateDate { get; private set; }
        public EOrderStatus Status { get; private set; }

        // COm o IReadyOnly somente consigo adicionar item pelo metodo 
        public IReadOnlyCollection<OrderItem> Items => _itens.ToArray();
        public IReadOnlyCollection<Delivery> Deliveries => _delivery.ToArray();



        public void AddItem(Prodct product, decimal quantity)
        {
            if (quantity > product.QuantityOnHand)
            {

                AddNotification("OrderItem", $"Produto {product.Title} não tem {quantity} itens em estoque.");

            }
            var item = new OrderItem(product, quantity);
            _itens.Add(item);

        }


        // public void AddDelivery(Delivery delivery) => _delivery.Add(delivery);




        // Criar o envio do pedido
        // to plave an Order
        public void Place()
        {

            // gera numero do pedido
            Number = new Guid().ToString().Replace("-", "").Substring(0, 8).ToUpper();

            if (_itens.Count == 0)
            {
                AddNotification("Order", "Pedido sem itens ");
            }




        }

        // pagar pedido
        public void Pay()
        {


            Status = EOrderStatus.Paid;


        }

        // enviar pedido
        // cada 5 produtos uma entrega separada
        public void Ship()
        {
            var deliveries = new List<Delivery>();
            var count = 0;
            foreach (var item in _itens)
            {


                if (count == 5)
                {
                    deliveries.Add(new Delivery(DateTime.Now.AddDays(5)));
                    count = 0;

                }
                count++;
            }
            // Envia todas as entregas
            deliveries.ForEach(x => x.Ship());

            // Adiciona as entregas ao pedido
            deliveries.ForEach(x => _delivery.Add(x));


            var delivery = new Delivery(DateTime.Now.AddDays(5));
            _delivery.Add(delivery);



        }




        // cancelar pedido
        public void Cancel()
        {

            Status = EOrderStatus.Cancelad;
            _delivery.ToList().ForEach(X => X.Cancel());


        }



    }
}