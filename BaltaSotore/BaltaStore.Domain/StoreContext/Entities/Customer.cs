using System;
using BaltaStore.Domain.StoreContext.ValueObjects;
using System.Collections.Generic;
using System.Linq;
using FluentValidator;
using FluentValidator.Validation;
using BaltaStore.Shared.Entities;

namespace BaltaStore.Domain.StoreContext.Entities
{
    public class Customer :  Entity
    {
        private readonly IList<Address> _address;

        public Customer(
            Name name,
            Document document,
            Email email,
            string phone
                
                  

        )
        {
            Name = name;
            Document = document;
            Email = email;
            Phone = phone;
            _address = new List<Address>();
            new ValidationContract()
            .Requires();
            
        }
    // com o private só cosegue setar os dados quando iniciar um customer
    // e passar os parametros na criação dele


    public Name Name { get; private set; }
    public Document Document { get; private set; }
    public Email Email { get; private set; }
    public string Phone { get; private set; }
    public IReadOnlyCollection<Address> Addresses => _address.ToArray();
    public void AddAddress(Address address) => _address.Add(address);

    // => equivale a usar o entre {  }
    public override string ToString()
    {
        // validar endereço e etc

        return Name.ToString();
    }



}
}