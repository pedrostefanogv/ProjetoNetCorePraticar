using System;

namespace BaltaStore.Domain.StoreContext.Queries
{


    public class ListCustomerQueryResult
    {

        public Guid Id { get; set; }

        public String Name { get; set; }

        public string Document { get; set; }


        public string Email { get; set; }




    }
}