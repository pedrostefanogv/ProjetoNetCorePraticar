using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.ValueObjects;
using BaltaStore.Domain.StoreContext.Entities;
using BaltaStore.Domain.StoreContext.Enums;

namespace BaltaStore.Testes
{
    [TestClass]

    public class OrderTests
    {

        private Prodct _mouse;
        private Prodct _keybord;
        private Prodct _chair;
        private Prodct _monitor;
        private Customer _customer;
        private Order _order;


        public OrderTests()
        {
            var name = new Name("Pedro", "Silva");
            var document = new Document("61117835219");
            var email = new Email("p3s_29@hotmail.com");
            _customer = new Customer(name, document, email, "33988024188");
            _order = new Order(_customer);

            _mouse = new Prodct("Mouse", "Mouse para computador", "mouse.png", 100M, 10);
            _chair = new Prodct("Chair", "Chair para computador", "Chair.png", 100M, 10);
            _monitor = new Prodct("MOnitor", "MOnitor para computador", "Monitor.png", 100M, 10);
            _keybord = new Prodct("Teclado", "Teclado para computador", "Teclado.png", 100M, 10);

        }


        // Consigo criar um novo pedido
        [TestMethod]
        public void ShouldCreateOrderWhenValid()
        {
            Assert.AreEqual(true, _order.Valid);

        }

        // Ao criar pedido, o status deve ser created
        [TestMethod]
        public void SatusBeCratedWhenOrderCreated()
        {
            Assert.AreEqual(EOrderStatus.Created, _order.Status);

        }

        // Ao adicionar um novo item, a quantidade deve mudar
        [TestMethod]
        public void ShouldReturnTwoWhenAddedTwoValidItens()
        {
            _order.AddItem(_monitor, 5);
            _order.AddItem(_mouse, 5);
            Assert.AreEqual(2, _order.Items.Count);


        }


        // Ao adicionar um novo item, deve subtrair a quantidade de produto
        [TestMethod]
        public void ShouldReturnFiveWhenAddedPurchaseFiveItem()
        {
            _order.AddItem(_mouse, 5);
            Assert.AreEqual(_mouse.QuantityOnHand, 5);
        }

        // Ao confirmar pedido, deve gerar um numero
        [TestMethod]
        public void ShouldReturnANumerWhenOrderPlaced()
        {
            _order.Place();
            Assert.AreNotEqual("", _order.Number);
        }

        // Ao pagar um pedido o status deve ser PAGO
        [TestMethod]
        public void ShouldReturnPaidWhenOrderPaid()
        {
            _order.Pay();
            Assert.AreEqual(EOrderStatus.Paid, _order.Status);
        }

        // Dado mais de 10 produtos, deve haver duas entregas
        [TestMethod]
        public void ShouldTwoWhenPurchaseTenProducts()
        {

            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.Ship();

            Assert.AreEqual(2, _order.Deliveries.Count);


        }


        // Ao cancelar o pedido, o status deve ser cancelado
        [TestMethod]

        public void StatusShouldBeCanceledWhenOrderCanceled()
        {
            _order.Cancel();
            Assert.AreEqual(EOrderStatus.Cancelad, _order.Status);

        }



        // Ao cancelar o pedido, deve cancelar as entregas
        [TestMethod]

        public void ShouldCancelShippingsWhenOrderCanceled()
        {
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.AddItem(_keybord, 1);
            _order.Ship();
            _order.Cancel();


            foreach (var X in _order.Deliveries)
            {
                Assert.AreEqual(EDeliveryStatus.Cancelad, X.Status);

            }

        }

        public void CreateCustomer()
        {   
            // Verificar se o CPF já existe
            // Verificar se o Email já existe
            // Criar os VOS
            // Criar as entidade
            // Validar as entidades e VO
            // inserir convite do sllack
            // envia um email de boas vindas


        }


    }
}



