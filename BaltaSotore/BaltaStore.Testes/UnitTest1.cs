using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.ValueObjects;
using BaltaStore.Domain.StoreContext.Entities;


namespace BaltaStore.Testes
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()

        {
            var name = new Name("Pedro", "Stefano");
            var document = new Document("12345678911");
            var email = new Email("p3s_29@hotmail.com");
            var c = new Customer(name, document, email, "199920714");


            var mouse = new Prodct("Mouse", "Rato", "image.png", 59.90M, 10);
            var teclado = new Prodct("Teclado", "Teclado", "image.png", 159.90M, 10);
            var impressora = new Prodct("Impressora", "Impressora", "image.png", 359.90M, 10);
            var cadeira = new Prodct("Cadeira", "Rato", "image.png", 559.90M, 10);

            var order = new Order(c);

            order.AddItem(mouse, 5);
            order.AddItem(teclado, 5);
            order.AddItem(cadeira, 5);
            order.AddItem(impressora, 5);



            // fiz o pedido
            order.Place();

            // siimular pagamento
            order.Pay();

            //simular Envio
            order.Ship();

            // simular cancelamento
            order.Cancel();

        }
    }
}
