using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.ValueObjects;


namespace BaltaStore.DocumentTests
{
    [TestClass]

    public class NameTests
    {
        [TestMethod]
        public void ShouldReturnNotificationWhenNametIsNotValid()
        {
            var name = new Name("Pedro", "Silva");
            Assert.AreEqual(false, name.Invalid);
            //Teste com erro
            Assert.AreEqual(0, name.Notifications.Count);



        }

        [TestMethod]
        public void ShouldReturnNotificationWhenNametIstValid()
        {
            var name = new Name("Pedro", "Silva");
            Assert.AreEqual(true, name.Valid);
            Assert.AreEqual(0, name.Notifications.Count);



        }






    }
}