using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.ValueObjects;


namespace BaltaStore.DocumentTests
{
    [TestClass]

    public class DocumentTests
    {

        private Document validDocument;
        private Document invalidDocument;

        public DocumentTests()
        {
            validDocument = new Document("61117835219");
            invalidDocument = new Document("12345678911");
        }




        [TestMethod]
        public void ShouldReturnNotificationWhenDocumentIsNotValid()
        {
           
            Assert.AreEqual(false, invalidDocument.Valid);
            Assert.AreEqual(1, invalidDocument.Notifications.Count);

        }


        [TestMethod]
        public void ShouldReturnNotificationWhenDocumentIsValid()
        {

            Assert.AreEqual(true, validDocument.Valid);
            Assert.AreEqual(0, validDocument.Notifications.Count);

        }




    }
}