using BaltaStore.Domain.StoreContext.CustomerCommands.Inputs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.Handlers;

namespace BaltaStore.Testes
{

    [TestClass]
    public class CustomerHandlerTests
    {
        [TestMethod]
        public void ShouldRegisterCustomerWhenCommandIsValid()
        {
            var command = new CreateCustomerCommand();
            command.FirstName = "Pedro";
            command.LastName = "Silva";
            command.Document = "61117835219";
            command.Email = "p3s_9@hotmail.com";
            command.Phone = "33999999999";


            Assert.AreEqual(true, command.Validate());


            var handler = new CustomerHandler(new FakeCustomerRepository(), new FakeEmailService());
            var result = handler.Handle(command);

            Assert.AreNotEqual(null, result);
            Assert.AreEqual(true, handler.Valid);

        }


    }




}