using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaltaStore.Domain.StoreContext.ValueObjects;
using BaltaStore.Domain.StoreContext.CustomerCommands.Inputs;

namespace BaltaStore.Commands
{
    [TestClass]

    public class CreateCustommerCommandtests
    {

        [TestMethod]
        public void ShouldValidateWhenCommandIsValid()
        {

            var command = new CreateCustomerCommand();
            command.FirstName = "Pedro";
            command.LastName = "Silva";
            command.Document = "61117835219";
            command.Email = "p3s_9@hotmail.com";
            command.Phone = "33999999999";


            Assert.AreEqual(true, command.Validate());

        }



    }
}