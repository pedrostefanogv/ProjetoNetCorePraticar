﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaltaStore.Domain.StoreContext.Services;
using BaltaStore.Domain.StoreContext.Repositories;
using BaltaStore.Infra.StoreContext.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using BaltaStore.Infra.StoreContext.DataContext;
using BaltaStore.Domain.StoreContext.Handlers;
using Swashbuckle.AspNetCore.Swagger;
using Elmah.Io.AspNetCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using BaltaStore.Shared;

namespace BaltaStore.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            // Pegar valor referencia ca configuração do arquivo jon
            Settings.ConnectionString = $"{Configuration["ConnectionString"]}";


            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsetings.json");

            Configuration = builder.Build();

            // Compactar requisições zzip
            services.AddResponseCompression();

            services.AddMvc();

            // Sempre que pedir um novo ele verifica se já existe um
            services.AddScoped<BaltaDataContext, BaltaDataContext>();


            // Sempre vai ser instanciado um novo, apos é destruido            
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<CustomerHandler, CustomerHandler>();


            services.AddSwaggerGen(X =>
            {
                X.SwaggerDoc("v1", new Info { Title = "Balta Store", Version = "1" });

            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseResponseCompression();
            app.UseMvc();


            app.UseSwagger();
            app.UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "BaltaStore - v1");
               });

            // colerar dados de erros que ocorrerem
            //  app.UseElmahIo("840979f6a8d1420dacc7718192b4219f", new Guid("58b2cfbb-2f90-47a8-bc00-97969020ef48"));

        }
    }
}
